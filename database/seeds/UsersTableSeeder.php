<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::insert(['name' => 'admin', 'email' => 'admin@admin.ru', 'password' => Hash::make('qwerty'), 'is_admin' => 1

        ]);
    }
}
