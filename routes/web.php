<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/delete-all', 'PostsController@deleteAll')->name('deleteall');
Route::delete('/delete-all', 'PostsController@deleteAll');

Route::get('/', 'PostsController@index')->name('index');
Route::resource('/comments', 'PostsController', ['except' => ['index', 'create', 'show']]);

Auth::routes();

Route::get('/user/{id}', 'PostsController@showUsersPosts')->name('usersPosts');
