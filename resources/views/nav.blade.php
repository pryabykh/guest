    <div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
  <h5 class="my-0 mr-md-auto font-weight-normal"><a href="/">Гостевая книга</a></h5>

  <nav class="my-2 my-md-0 mr-md-3">
@if(session('status') == 0)
<a class="btn btn-outline-primary" href="{{ route('login') }}">Вход</a>
<a class="btn btn-outline-primary" href="{{ route('register') }}">Регистрация</a>
@endif

@if(session('status') >= 1)
 <font color='gray'>{{ session('user') }}</font> 
@endif

@if(session('status') == 2)
  <a class="btn btn-outline-danger" href="{{ route('deleteall') }}">Очистить комментарии</a>
@endif

   </nav>

 @if(session('status') >= 1)
 <form method="POST" action="{{ route('logout') }}">
 	@csrf
  <input type="submit" class="btn btn-outline-primary" value="Выйти">
</form>
@endif
</div>
