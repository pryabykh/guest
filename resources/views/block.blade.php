<div class="card-header">
       <font class="name_author"><a href="{{ route('usersPosts', ['id' => $post->user->id]) }}">{!! $post->user->name !!}</a></font> <font color='gray'>{!! $post->created !!}</font>
       @if(session('status') == 2)
       <font color="grey"><a href="mailto:{{ $post->email }}">{{ $post->user->email }}</a></font>
       @endif
      </div>
      <div class="card-body">

          <p>{!! $post->text !!}</p>

 @if(session('status') == 2 || $user_id == $post->user->id)         



<div class="row">
     <div>
<form action="{{ route('comments.destroy', ['comment' => $post->id]) }}" method="POST">
@csrf{{ method_field('DELETE') }}
<input class="btn btn-link edited-link" type="submit" value="удалить">
</form></div> 
<div>
<form action="{{ route('comments.edit', ['comment' => $post->id]) }}" method="POST">
@csrf{{ method_field('GET') }}
<input class="btn btn-link edited-link" type="submit" value="изменить">
</form></div>

</div>


@endif

 </div>
