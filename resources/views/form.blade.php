@if(session('status') > 0)
  <div class="container">
    <label for="exampleFormControlInput1"><font color="grey">Добавить комментарий</font></label>
<form method="POST" action="{{ route('comments.store') }}">
  @csrf
  <div class="form-group">
    <textarea class="form-control" id="exampleFormControlTextarea1" name="text" rows="5">{{ old('text') }}</textarea>
  </div>
   <input type="submit" class="btn btn-primary" value="Добавить">
</form>
</div>
@else
  <div class="container">
    <label for="exampleFormControlInput1"><a href="{{ route('login') }}">Войдите</a>, чтобы оставить комментарий или <a href="{{ route('register') }}">зарегистрируйтесь</a>.</font></label>
</div>
@endif