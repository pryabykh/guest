@extends('layout')

@section('content')
@if(count($data) > 0)
<div class="container">
  <div class="card-deck mb-3">
    <div class="card mb-4 shadow-sm">
    	
	    	@foreach($data as $post)
			@include('block')
			@endforeach
    </div>
    </div>
  </div>
  <div class="container">
      {{ $data->links() }}
  </div>
@else
@include('notposts');
@endif
 @endsection