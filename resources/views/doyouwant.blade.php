@extends('layout')

@section('content')

@endsection

@section('form')
  <div class="container">

  <label for="exampleFormControlInput1"><b>Вы уверены, что хотите удалить все записи из Гостевой книги?<br>То, что случится, уже не вернуть...</b></label>
  <div class="container">
<div class="row">
    <div class="col-xs-2 border col-xs-offset-4">
<form method="POST">
	  @csrf
{{ method_field('DELETE') }}
<input class="btn btn-outline-danger" type="submit" value="Да">
</form>
    </div>
    <div class="col-xs-2"> </div>
    <div class="col-xs-2"><a class="btn btn-outline-primary" href="{{ route('index') }}">Нет</a></div>
</div>
</div>

  </div>
@endsection