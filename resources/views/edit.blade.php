@extends('layout')

@section('content')

@endsection

@section('form')
  <div class="container">
    <label for="exampleFormControlInput1"><font color="grey">Редактировать комментарий</font></label>
<form action="{{ route('comments.update', ['comment' => $data->id]) }}" method="POST">
	  @csrf
{{ method_field('PUT') }}
  <div class="form-group">
    <textarea class="form-control" id="exampleFormControlTextarea1" name="text" rows="5">{!! $data->text !!}</textarea>
  </div>
   <input type="submit" class="btn btn-primary" value="Изменить">
</form>
</div>
@endsection