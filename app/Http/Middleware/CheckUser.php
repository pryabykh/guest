<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use App\User;
use App\Post;
use Closure;

class CheckUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $uniqid_post = Post::select('uniqid')->where('id', $request->id)->first();
        $uniqid_post = $uniqid_post->uniqid;
        $user = Auth::user()->name ?? '';
        $uniqid = $_COOKIE['uniqid'] ?? '';

        if($user == 'admin' || $uniqid == $uniqid_post) {
            return $next($request);
        } else {
            return redirect('/');
        }

        
    }
}
