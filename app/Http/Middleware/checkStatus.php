<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\User;

class checkStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()) {
            $status = Auth::user()->is_admin ? 2 : 1;
            $request->session()->put('status', $status);
            
            $user_name = Auth::user()->name;
            $request->session()->put('user', $user_name);
        } else {
            $status = 0;
            $request->session()->put('status', $status);
        }
        return $next($request);
    }
}
