<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class PostRequest extends FormRequest
{
    protected function getRedirectUrl()
    {
        $url = $this->redirector->getUrlGenerator();

        return $url->previous().'#errors';
    }

    public static function filter($data)
    {   
        $stops = ['хуй', 'пизда', 'блять', 'ебать', 'пидор', 'гандон'];
        $status = false;
    foreach($data as $text){
        foreach($stops as $stop)
        {
            if(strpos(mb_strtolower($text), $stop) !== FALSE){
                $status = true;
                return $status;
            }
        }
    }
        return $status;
    }

    public function withValidator($validator)
    {
        $validator->after(function($validator) {
            if($this->filter($_POST)) {
                $validator->errors()->add('censor', 'Использование нецензурной лексики недопустимо');
            }
        });
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {   
        return true;
    }

    public function messages()
    {
        return [
            'text.required' => 'Поле Комментарий обязательно к заполнению',
            'text.max' => 'Поле Комментарий не должно превышать :max символов'
        ];
    }

    public function rules()
    {
        return [
            'text' => 'required|max:3000',
        ];
    }
}
