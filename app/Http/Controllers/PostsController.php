<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;
use App\Post;
use App\User;
use App\Classes\CookieSetter;
use App\Http\Requests\PostRequest;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user_id = Auth::user()->id ?? '';
        $msgStatus = $request->session()->pull('msg');
        $data = Post::orderBy('id', 'desc')->paginate(5);
        return view('index', compact('data', 'msgStatus', 'user_id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request)
    {
        $data = $request->except('_token');
        $data['created'] = date('d.m.Y H:i');
        $data['user_id'] = Auth::user()->id;

        foreach($data as $key => $item) {
            $data[$key] = nl2br(htmlspecialchars($item));
        }       

        if(Post::create($data)) {
            return redirect()->route('index')->with('msg', 'Комментарий успешно добавлен');
        } else {
            return redirect()->route('index')->with('msg', 'Неопознанная ошибка');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $request->session()->put('pageurl', $request->server('HTTP_REFERER'));
        $user = Auth::user()->name ?? '';
        $data = Post::where('id', $id)->first();
        $data->text = strip_tags($data->text);

        return view('edit', compact('data', 'user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PostRequest $request, $id)
    {
        $url = $request->session()->pull('pageurl') ?? route('index');
        $data = $request->except('_token', '_method');
        $data['edited'] = date('d.m.Y H:i');

        foreach($data as $key => $item) {
            $data[$key] = nl2br(htmlspecialchars($item));
        }
        if(Post::where('id', $id)->update($data)){
            return redirect($url)->with('msg', 'Запись успешно изменена');
        } else {
            return redirect()->route('index')->with('msg', 'Неопознанная ошибка');
            }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {           
                if(Post::where('id', $id)->delete()) {
                return back()->with('msg', 'Запись успешно удалена');
        } else {
                return redirect()->route($url)->with('msg', 'Неопознанная ошибка');

        }
    }

            public function deleteAll(Request $request)
        {   
            if($request->isMethod('GET')) {
                $user = Auth::user()->name ?? '';
                return view('doyouwant', compact('user'));
            } elseif($request->isMethod('DELETE')) {
                Post::where('id', '>', 0)->delete();

                return redirect()->route('index')->with('msg', 'Все записи успешно удалены');
            }
        }

        public function showUsersPosts(Request $request, $id)
        {  
            $user_id = Auth::user()->id ?? '';
            $msgStatus = $request->session()->pull('msg');
            $user = User::find($id);
            $data = $user->posts()->orderBy('id', 'desc')->paginate(5);
            return view('index', compact('data', 'msgStatus', 'user_id'));
        }
}
